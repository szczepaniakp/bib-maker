import crypt
import hashlib
import json
import os
from datetime import timedelta
from functools import wraps

import redis
from authlib.integrations.flask_client import OAuth
from flask import (Flask, make_response, redirect, render_template, request,
                   session, url_for)

GET = "GET"
POST = "POST"
USERNAMES = "usernames"
NICKNAME = "nickname"
SECRET_KEY = "BM_SECRET"
PASSWORD_SALT = "BIB_MAKER_DB_SALT"
TOKEN_EXPIRES_IN_SECONDS = 300
REDIS_SALT = os.environ.get(PASSWORD_SALT)

OAUTH_BASE_URL = "https://dev--2xij6hw.eu.auth0.com"
OAUTH_ACCESS_TOKEN_URL = OAUTH_BASE_URL + "/oauth/token"
OAUTH_AUTHORIZE_URL = OAUTH_BASE_URL + "/authorize"
OAUTH_CALLBACK_URL = "https://localhost:8080/callback"
OAUTH_CLIENT_ID = "BCq1BrhnGLy3KX3rvYa00MMembffLMg8"
OAUTH_CLIENT_SECRET = os.environ.get("OAUTH_CLIENT_SECRET")
OAUTH_SCOPE = "openid profile email"

app = Flask(__name__, static_url_path="")
app.secret_key = os.environ.get(SECRET_KEY)

db = redis.Redis(host="redis", port=6379, decode_responses=True)

oauth = OAuth(app)
auth0 = oauth.register(
    "BM-auth0",
    api_base_url=OAUTH_BASE_URL,
    client_id=OAUTH_CLIENT_ID,
    client_secret=OAUTH_CLIENT_SECRET,
    access_token_url=OAUTH_ACCESS_TOKEN_URL,
    authorize_url=OAUTH_AUTHORIZE_URL,
    client_kwargs={"scope": OAUTH_SCOPE})


@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(
        minutes=TOKEN_EXPIRES_IN_SECONDS/60)


@app.route("/")
def index():
    response = make_response(render_template("index.html"))

    return response


@app.route("/login", methods=[GET])
def login():
    return auth0.authorize_redirect(redirect_uri=OAUTH_CALLBACK_URL, audience="")


@app.route("/callback")
def oauth_callback():
    auth0.authorize_access_token()

    resp = auth0.get("userinfo")
    login = resp.json()["email"]
    session[NICKNAME] = login

    if login not in db.hkeys(USERNAMES):
        userdata = {
            "id": int(db.hlen(USERNAMES)),
            "filenames": {},
            "bibs": {}
        }

        db.hset(USERNAMES, login, json.dumps(userdata))

    return redirect("/")


def authorization_required(f):
    @wraps(f)
    def authorization_decorator(*args, **kwds):
        if NICKNAME not in session:
            return redirect("https://localhost:8080/login")

        return f(*args, **kwds)

    return authorization_decorator


@app.route("/logout", methods=[GET])
@authorization_required
def logout():
    url_params = "returnTo=" + url_for("logout_info", _external=True)
    url_params += "&"
    url_params += "client_id=" + OAUTH_CLIENT_ID

    session.clear()
    return redirect(auth0.api_base_url + "/v2/logout?" + url_params)


@app.route("/logout_info")
def logout_info():
    return render_template("secure/logout.html")


@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error=error)


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error=error)


@app.errorhandler(400)
def page_bad_request(error):
    return render_template("errors/400.html", error=error)


@app.errorhandler(401)
def page_unauthorized(error):
    return render_template("errors/401.html", error=error)
