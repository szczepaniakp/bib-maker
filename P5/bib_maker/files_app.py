import datetime
import json
import os
import sys
import pika
import redis

from flask import (Flask, make_response, redirect, render_template, request,
                   send_file, session, url_for)
from flask_socketio import SocketIO, emit, join_room, leave_room, send
from jwt_app import authorization_required


GET = "GET"
POST = "POST"
SECRET_KEY = "BM_SECRET"
TOKEN_EXPIRES_IN_SECONDS = 300

NICKNAME = "nickname"
USERNAMES = "usernames"
DIR_PATH = "files/"
FILE_COUNTER = "file_counter"
BIB_COUNTER = "bib_counter"
ORG_FILENAME = "org_filename"
NEW_FILENAME = "new_filename"
PATH_TO_FILE = "path_to_file"
FILENAMES = "filenames"
PUBLIC_FILENAMES = "public_filenames"
UPLOAD_DATE = "upload_date"
ORG_BIB_TITLE = "org_bib_title"
BIB_DESC = "bib_description"
AUTHOR = "author"
YEAR = "year"
BIBS = "bibs"

USER_AGENT = "useragent"
ROOM_ID = "room_id"

app = Flask(__name__, static_url_path="")
app.secret_key = os.environ.get(SECRET_KEY)
socket_io = SocketIO(app)

db = redis.Redis(host="redis", port=6379, decode_responses=True)


@app.route("/files", methods=[GET])
@authorization_required
def files():
    public_files = dict()
    userdata = json.loads(db.hget(USERNAMES, session[NICKNAME]))["filenames"]

    try:
        public_files = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    except:
        app.logger.debug("Brak plików publicznych.")

    response = make_response(render_template(
        "secure/index-files.html", user_files=userdata, public_files=public_files))
    return response


@app.route("/bibs", methods=[GET])
@authorization_required
def bibs(files_to_pin={}):
    userdata = json.loads(db.hget(USERNAMES, session[NICKNAME]))

    try:
        files_to_pin = json.loads(request.args['files_to_pin'])
    except:
        app.logger.debug("Renderowanie bez plików do podpięcia.")

    response = make_response(render_template(
        "secure/index-bibs.html", bibs=userdata[BIBS], files_to_pin=files_to_pin))

    return response


@app.route("/add-bib", methods=[GET, POST])
@authorization_required
def add_bib():
    if request.method == GET:
        return make_response(render_template("secure/add-bib.html"))

    elif request.method == POST:
        title = request.form["title"]
        author = request.form["author"]
        year = request.form["year"]
        desc = ""
        login = session[NICKNAME]

        try:
            desc = request.form["description"]
        except Exception as e:
            app.logger.debug("Brak opisu pozycji.")

        data = {
            ORG_BIB_TITLE: title,
            AUTHOR: author,
            YEAR: year,
            BIB_DESC: desc,
            UPLOAD_DATE: datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S"),
            FILENAMES: {}
        }

        save_bib_to_user_data(data, login)
        
        socket_io.emit("notification", {
                       "message": f"Publikacja \"{title}\" została dodana."}, room=login)

        return redirect(url_for('bibs'))

    else:
        page_bad_request("Niepoprawna metoda.")


def save_bib_to_user_data(data, login):
    new_bib_name = str(db.incr(BIB_COUNTER)) + "-" + data[ORG_BIB_TITLE]

    userdata = json.loads(db.hget(USERNAMES, login))
    userdata[BIBS][new_bib_name] = data
    db.hset(USERNAMES, login, json.dumps(userdata))


@app.route("/add-file", methods=[GET, POST])
@authorization_required
def add_file():
    if request.method == GET:
        return make_response(render_template("secure/add-file.html"))

    elif request.method == POST:
        is_public = False
        try:
            is_public = True if str(
                request.form["publicSwitch"]) == "on" else False
        except Exception as e:
            print(str(e))

        login = session[NICKNAME]
        filename = request.files["file"].filename
        saved_filename = save_file(request.files["file"], login, is_public)

        app.logger.debug(f"PLIK NAZYWA SIE: {saved_filename}")

        if saved_filename is None:
            page_bad_request("Niepoprawny plik PDF.")
        elif ".pdf" in saved_filename:
            init_consumer()

            add_to_queue(login, saved_filename)
            app.logger.debug(f"Plik '{filename}' został dodany do kolejki")

        socket_io.emit("notification", {
                       "message": f"Plik \"{filename}\" został dodany."}, room=login)

        return redirect(url_for('files'))
    else:
        page_bad_request("Niepoprawna metoda.")


@app.route("/pin-file/<string:bib_hash>", methods=[POST])
@authorization_required
def pin_file(bib_hash):
    filenames = request.form.getlist('file-selector')
    files_to_pin = {}
    login = session[NICKNAME]
    userdata = json.loads(db.hget(USERNAMES, login))
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    for filename in filenames:
        if filename in userdata[FILENAMES]:
            file = userdata[FILENAMES][filename]
        elif filename in public_data:
            file = public_data[filename]

        files_to_pin[filename] = file

    userdata[BIBS][bib_hash][FILENAMES].update(files_to_pin)
    db.hset(USERNAMES, login, json.dumps(userdata))

    return redirect(url_for('bibs'))


@app.route("/show-files-to-pin/<string:bib_hash>/", methods=[GET])
@authorization_required
def show_files_to_pin(bib_hash):
    login = session[NICKNAME]
    userdata = json.loads(db.hget(USERNAMES, login))
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    data_to_send = {}
    data_to_send.update(userdata[FILENAMES])
    data_to_send.update(public_data)

    already_pinned = json.loads(db.hget(USERNAMES, login))[
        BIBS][bib_hash][FILENAMES]

    for key in already_pinned.keys():
        data_to_send.pop(key, None)

    return redirect(url_for('bibs', files_to_pin=json.dumps({bib_hash: data_to_send})))


@app.route("/download-file/<string:file_hash>", methods=[GET])
@authorization_required
def return_files(file_hash):
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    if file_hash in public_data:
        try:
            full_name = public_data[file_hash][PATH_TO_FILE]
            return send_file(full_name)

        except Exception as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie"{}".'.format(file_hash))
    else:
        try:
            userdata = json.loads(db.hget(USERNAMES, session[NICKNAME]))
            full_name = userdata["filenames"][file_hash][PATH_TO_FILE]
            return send_file(full_name)

        except Exception as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie"{}".'.format(file_hash))


@app.route("/delete-file/<string:file_hash>", methods=[GET])
@authorization_required
def delete_file(file_hash):
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    if file_hash in public_data:
        try:
            del public_data[file_hash]
            db.hset(FILENAMES, PUBLIC_FILENAMES, json.dumps(public_data))
            os.system("rm \"" + DIR_PATH + file_hash + "\"")

        except Exception as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie "{}".'.format(file_hash))

    else:
        login = session[NICKNAME]
        userdata = json.loads(db.hget(USERNAMES, login))
        try:
            del userdata[FILENAMES][file_hash]
            db.hset(USERNAMES, login, json.dumps(userdata))
            os.system("rm \"" + DIR_PATH + file_hash + "\"")

        except KeyError as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie "{}".'.format(file_hash))

    return redirect(url_for('files'))


@app.route("/delete-bib/<string:bib_hash>", methods=[GET])
@authorization_required
def delete_bib(bib_hash):
    login = session[NICKNAME]
    userdata = json.loads(db.hget(USERNAMES, login))

    try:
        del userdata[BIBS][bib_hash]
        db.hset(USERNAMES, login, json.dumps(userdata))

    except KeyError as e:
        app.logger.debug(str(e))
        return page_bad_request('Pozycja bibliograficzna o nazwie "{}" nie istnieje.'.format(bib_hash))

    return redirect(url_for('bibs'))


@app.route("/unpin-file/<string:bib_hash>/<string:file_hash>", methods=[GET])
@authorization_required
def unpin_file(bib_hash, file_hash):
    login = session[NICKNAME]
    userdata = json.loads(db.hget(USERNAMES, login))

    try:
        del userdata[BIBS][bib_hash][FILENAMES][file_hash]
        db.hset(USERNAMES, login, json.dumps(userdata))

    except KeyError as e:
        app.logger.debug(str(e))
        return page_bad_request('Pozycja bibliograficzna o nazwie "{}" nie zawiera pliku "{}".'.format(bib_hash, file_hash))

    return redirect(url_for('bibs'))


def save_file(file_to_save, login, is_public=True):
    if len(file_to_save.filename) > 0:

        new_filename = str(db.incr(FILE_COUNTER)) + "-" + file_to_save.filename
        path_to_file = DIR_PATH + new_filename

        data = {
            ORG_FILENAME: file_to_save.filename,
            PATH_TO_FILE: path_to_file,
            UPLOAD_DATE: datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        }

        if is_public:
            public_filenames = db.hget(FILENAMES, PUBLIC_FILENAMES)
            public_data = json.loads(
                public_filenames) if public_filenames is not None else {}
            public_data[new_filename] = data
            db.hset(FILENAMES, PUBLIC_FILENAMES, json.dumps(public_data))

        else:
            userdata = json.loads(db.hget(USERNAMES, login))
            userdata["filenames"][new_filename] = data
            db.hset(USERNAMES, login, json.dumps(userdata))

        file_to_save.save(path_to_file)
        return new_filename

    else:
        print("\n\t\t[WARN] Empty content of file\n", file=sys.stderr)
        return None


@socket_io.on("connect")
def handle_on_connect():
    app.logger.debug("Connected -> OK")
    emit("connection response", {"data": "Correctly connected"})


def handle_on_disconnect():
    app.logger.debug("Disconnected")


@socket_io.on("join")
@authorization_required
def handle_on_join(data):
    useragent = data[USER_AGENT]
    room_id = session[NICKNAME]
    join_room(room_id)
    emit("joined_room", {"room_id": room_id})
    app.logger.debug("Useragent: %s added to the room: %s" %
                     (useragent, room_id))


@authorization_required
@socket_io.on("leave")
def handle_on_leave(data):
    useragent = data[USER_AGENT]
    leave_room(session[NICKNAME])
    app.logger.debug("Useragent: %s is no longer in the room: %s" %
                     (useragent, room_id))


@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error=error)


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error=error)


@app.errorhandler(400)
def page_bad_request(error):
    return render_template("errors/400.html", error=error)


@app.errorhandler(401)
def page_unauthorized(error):
    return render_template("errors/401.html", error=error)


if __name__ == '__main__':
    from consumer import add_to_queue, init_consumer

    db.hset(FILENAMES, PUBLIC_FILENAMES, json.dumps({}))
    socket_io.run(app, host="0.0.0.0", port=81, debug=True,
                  keyfile='key.pem', certfile='cert.pem')
