from flask import Flask, request
import datetime
import pika, json
from threading import Thread
import os
from PyPDF2 import PdfFileReader
from files_app import save_bib_to_user_data, ORG_BIB_TITLE, BIB_DESC, AUTHOR, YEAR, FILENAMES, UPLOAD_DATE, DIR_PATH

QUEUE_NAME = "pdfs_to_process_queue"
DELIVERY_MODE_PERSISTENT = 2
NUM_OF_TASK_PER_CONSUMER = 1

app = Flask(__name__, static_url_path = "")

def init_consumer():
    consumer__queue_thread = Thread(target = consume_messages)
    consumer__queue_thread.start()

def open_channel():        
    rabbit_mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
    channel = rabbit_mq_connection.channel()
    channel.queue_declare(queue = QUEUE_NAME, durable = True)
    return channel

def add_to_queue(login, filename):
    channel = open_channel()
    channel.basic_publish(
        exchange = "",
        routing_key = QUEUE_NAME,
        body = json.dumps({AUTHOR : login, FILENAMES : filename}),
        properties = pika.BasicProperties(delivery_mode = DELIVERY_MODE_PERSISTENT))


def callback_mq(channel, method, properties, body):
    file_info = json.loads(body)

    app.logger.debug("Filename from queue %s" % file_info[FILENAMES])
    data = extract_data_from_pdf(file_info[FILENAMES])
    app.logger.info("[DONE] File in queue has been processed (%s)" % file_info[FILENAMES])

    save_bib_to_user_data(data, file_info[AUTHOR])
    app.logger.info("[DONE] Nowa pozycja bibliograficzna została dodana (%s)" % file_info[FILENAMES])

    channel.basic_ack(delivery_tag = method.delivery_tag)

def extract_data_from_pdf(pdf_filename):
    data = {}

    try:
        src = DIR_PATH + pdf_filename
        pdf = PdfFileReader(open(src, 'rb'))
        pdf_info = pdf.getDocumentInfo()

        app.logger.debug("pdf_info: \n{}\n".format(pdf_info))

        data[ORG_BIB_TITLE] = pdf_filename
        data[BIB_DESC] = "(wygenerowane z pliku PDF)"
        data[UPLOAD_DATE] = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        data[FILENAMES] = {}
        data[YEAR] = pdf_info["/CreationDate"][2:6]
        data[AUTHOR] = pdf_info["/Author"]
        
    except:
        app.logger.error("[ERROR] During extracting data from pdf (%s)" % pdf_filename)

    return data

def consume_messages():
    channel = open_channel()
    channel.basic_qos(prefetch_count = NUM_OF_TASK_PER_CONSUMER)
    channel.basic_consume(queue = QUEUE_NAME, on_message_callback = callback_mq)
    channel.start_consuming()
