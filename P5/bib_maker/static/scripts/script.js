document.addEventListener("DOMContentLoaded", function (event) {

    var ws_uri = "https://" + document.domain + ":" + location.port;
    var socket = io.connect(ws_uri);
    useragent = navigator.userAgent;

    socket.on("connect", function () {
        console.log("Correctly connected websocket");
    });

    socket.on("joined_room", function (message) {
        console.log(message.room_id);
    });

    socket.on("notification", function (message) {
        console.log(message.message);
        $.notify(message.message, "info");
    });

    socket.emit("join", {useragent: useragent});

    //socket.emit("leave, {useragent: useragent}")
});
