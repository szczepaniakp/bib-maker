from flask import Flask, request, render_template
import redis
from flask_jwt_extended import JWTManager, create_access_token, jwt_required
import os

GET = "GET"
POST = "POST"
SECRET_KEY = "LAB_5_SECRET"
TOKEN_EXPIRES_IN_SECONDS = 30

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host = "redis", port = 6379, decode_responses = True)

app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)

app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS
jwt = JWTManager(app)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/login", methods = [POST])
def login():
    username = request.form["username"]
    access_token = create_access_token(identity = username)
    return {"access_token" : access_token}

@app.route("/secret", methods = [GET])
@jwt_required
def secret():
    return {"secret-info" : "123456789"}