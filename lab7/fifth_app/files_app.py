from flask import Flask, request, render_template, send_file
import redis
from flask_jwt_extended import JWTManager, create_access_token, jwt_required
import os, sys

GET = "GET"
SECRET_KEY = "LAB_5_SECRET"
TOKEN_EXPIRES_IN_SECONDS = 30

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host = "redis", port = 6379, decode_responses = True)

app.config['JWT_SECRET_KEY'] = os.environ.get(SECRET_KEY)

app.config['JWT_ACCESS_TOKEN_EXPIRES'] = TOKEN_EXPIRES_IN_SECONDS
jwt = JWTManager(app)

@app.route("/")
def index():
    return render_template("index-files.html")

@app.route("/secret-files", methods = [GET])
@jwt_required
def secret():
    return {"secret-info" : "to jest strona z tajnymi dokumentami PDF"}

@app.route('/download-files/', methods = [GET])
@jwt_required
def return_files_tut():
    try:
        return send_file('files/cos.png')
    except Exception as e:
        return print(str(e), file = sys.stderr)