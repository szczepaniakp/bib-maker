## P1
Aby zbudować należy użyć "docker build -t=bibmaker_app .". 
Kontener uruchamiamy poleceniem "docker run --rm -it -p80:80 bibmaker_app" będąc w folderze "P1/lab2". Następnie należy uruchomić przeglądarkę i wpisać "localhost:80".

## P2 
Uruchamiamy poleceniem "docker-compose up" będąc w folderze "P2". Następnie należy uruchomić przeglądarkę i wpisać "localhost:8080".

## P3 
Najpierw należy też ustawić dwie zmienne środowiskowe: BM_SECRET i BIB_MAKER_DB_SALT.
Uruchamiamy poleceniem "docker-compose up" będąc w folderze "P3". Następnie należy wpisać w przeglądarkę "https://localhost:8080".

## P4
Najpierw należy ustawić zmienne środowiskowe: BM_SECRET, BIB_MAKER_DB_SALT oraz OAUTH_CLIENT_SECRET.
Uruchamiamy poleceniem "docker-compose up" będąc w folderze "P4". Następnie należy wpisać w przeglądarkę "https://localhost:8080".

## P5
Należy ustawić zmienne środowiskowe: BM_SECRET, BIB_MAKER_DB_SALT oraz OAUTH_CLIENT_SECRET (w związku z OAUTH_CLIENT_SECRET -- ta sama sytuacja co w P4).
Następnie należy uruchamić poleceniem "docker-compose up --build" będąc w folderze "P5" i wpisać w przeglądarkę "https://localhost:8080".
