import os

OAUTH_BASE_URL = "https://dev--2xij6hw.eu.auth0.com"
OAUTH_ACCESS_TOKEN_URL = OAUTH_BASE_URL + "/oauth/token"
OAUTH_AUTHORIZE_URL = OAUTH_BASE_URL + "/authorize"
OAUTH_CALLBACK_URL = "http://localhost:8080/callback"
OAUTH_CLIENT_ID = "BCq1BrhnGLy3KX3rvYa00MMembffLMg8"
OAUTH_CLIENT_SECRET = os.environ.get("OAUTH_CLIENT_SECRET")
OAUTH_SCOPE = "openid profile"
SECRET_KEY = os.environ.get("BM_SECRET")
NICKNAME = "nickname"