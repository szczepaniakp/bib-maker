from flask import Flask, render_template, session, url_for, redirect
from authlib.flask.client import OAuth
from functools import wraps
from const_config import *
import logging

app = Flask(__name__, static_url_path = "")
app.secret_key = SECRET_KEY
oauth = OAuth(app)

auth0 = oauth.register(
        "lab-12-auth0",
        api_base_url = OAUTH_BASE_URL,
        client_id = OAUTH_CLIENT_ID,
        client_secret = OAUTH_CLIENT_SECRET,
        access_token_url = OAUTH_ACCESS_TOKEN_URL,
        authorize_url = OAUTH_AUTHORIZE_URL,
        client_kwargs = {"scope": OAUTH_SCOPE} )

def authorization_required(f):
    @wraps(f)
    def authorization_decorator(*args, **kwds):
        if NICKNAME not in session:
            return redirect("/login")

        return f(*args, **kwds)

    return authorization_decorator

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/login")
def login():
    return auth0.authorize_redirect(
            redirect_uri = OAUTH_CALLBACK_URL, 
            audience = "")

@app.route("/logout_info")
def logout_info():
    return render_template("./secure/logout.html")

@app.route("/logout")
def logout():
    url_params = "returnTo=" + url_for("logout_info", _external = True)
    url_params += "&"
    url_params += "client_id=" + OAUTH_CLIENT_ID

    session.clear()
    return redirect(auth0.api_base_url + "/v2/logout?" + url_params)

@app.route("/secure")
@authorization_required
def secure():
    return render_template("./secure/secure_page.html")

@app.route("/callback")
def oauth_callback():
    auth0.authorize_access_token()
    resp = auth0.get("userinfo")
    nickname = resp.json()["nickname"]
    logging.warning("NICKNAME")
    logging.warning(nickname)

    session[NICKNAME] = nickname

    return redirect("/secure")