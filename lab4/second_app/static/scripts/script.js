document.addEventListener('DOMContentLoaded', function (event) {

    let addingForm = document.getElementById("adding-form");

    addingForm.addEventListener("submit", function (event) {
        event.preventDefault();

        var data = new FormData();
        data.append("title", document.getElementById("title").value);
        data.append("author", document.getElementById("author").value);
        data.append("year", document.getElementById("year").value);
        data.append("publisher", document.getElementById("publisher").value);

        var xhttp = new XMLHttpRequest();
        xhttp.addEventListener("load", function () {
            if (xhttp.status == "201") {
                console.log("Dodano pozycję.");
                location.reload()
            } 
            console.log(xhttp.status);
        });

        xhttp.open("POST", "http://0.0.0.0:80/bookApi");
        xhttp.setRequestHeader("Accept", "*/*");
        xhttp.setRequestHeader("Access-Control-Allow-Origin", "*");
        xhttp.send(data)
    });
});
