from flask import Flask, request
from flask import jsonify, render_template
from src.book.book import Book

app = Flask(__name__)

books = []

@app.route("/", methods=["GET"])
def home():
    return render_template("index.html", my_books = books)

@app.route("/bookApi/", methods=["POST"])
def add_book():
    book = to_book(request.form)
    books.append(book)
    return jsonify({"title": book.title}), 201

def to_book(request):
    return Book(request.get("author"), request.get("title"), request.get("year"), request.get("publisher"))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
