from flask import Flask, request
import pika
from threading import Thread
import os, logging
from PyPDF2 import PdfFileReader


PDFS_DIR_PATH = "PDF_files/"
PDFS_DIR_IN_QUEUE = PDFS_DIR_PATH + "in_queue/"
PDFS_DIR_PROCESSED = PDFS_DIR_PATH + "processed/"

CODING = "utf-8"

QUEUE_NAME = "pdfs_to_process_queue"
DELIVERY_MODE_PERSISTENT = 2
NUM_OF_TASK_PER_CONSUMER = 1

app = Flask(__name__, static_url_path = "")

def init():
    consumer__queue_thread = Thread(target = consume_messages)
    consumer__queue_thread.start()
    
def add_to_queue(filename):
    channel = open_channel()
    channel.basic_publish(
        exchange = "",
        routing_key = QUEUE_NAME,
        body = filename,
        properties = pika.BasicProperties(delivery_mode = DELIVERY_MODE_PERSISTENT))

def open_channel():
    rabbit_mq_connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
    channel = rabbit_mq_connection.channel()
    channel.queue_declare(queue = QUEUE_NAME, durable = True)
    return channel


def callback_mq(channel, method, properties, body):
    app.logger.debug("Filename from queue %s" % body)
    pdf_title = extract_data_from_pdf(body)
    app.logger.debug(pdf_title)
    move_file_to_processed_dir(body)
    app.logger.info("[DONE] File in queue has been processed (%s)" % body)
    channel.basic_ack(delivery_tag = method.delivery_tag)

def extract_data_from_pdf(pdf_filename):
    pdf_title = ""
    try:
        src = PDFS_DIR_IN_QUEUE + str(pdf_filename.decode(CODING))
        pdf = PdfFileReader(open(src, 'rb'))
        pdf_info = pdf.getDocumentInfo()
        app.logger.debug(pdf_info)
        pdf_title = str(pdf_info.title)
    except:
        app.logger.error("[ERROR] During extracting data from pdf (%s)" % pdf_filename)

    return pdf_title

def move_file_to_processed_dir(filename_in_queue):
    src = PDFS_DIR_IN_QUEUE + str(filename_in_queue.decode(CODING))
    dest = PDFS_DIR_PROCESSED + str(filename_in_queue.decode(CODING))
    try:
        os.rename(src, dest)
        app.logger.debug("Moved file %s" % filename_in_queue)
    except FileNotFoundError as e:
        app.logger.error(e)

def consume_messages():
    channel = open_channel()
    channel.basic_qos(prefetch_count = NUM_OF_TASK_PER_CONSUMER)
    channel.basic_consume(queue = QUEUE_NAME, on_message_callback = callback_mq)
    channel.start_consuming()


# info o kolejkach, bardziej szczegołowe, z prezentacji
#
#if __name__ == '__main__':
#    app.run(host="0.0.0.0", port=81)
#
