from flask import Flask, request
import pika
from threading import Thread
import os, logging
from PyPDF2 import PdfFileReader
from cons import add_to_queue, init

PDFS_DIR_PATH = "PDF_files/"
PDFS_DIR_IN_QUEUE = PDFS_DIR_PATH + "in_queue/"
PDFS_DIR_PROCESSED = PDFS_DIR_PATH + "processed/"

GET = "GET"
POST = "POST"
CODING = "utf-8"

QUEUE_NAME = "pdfs_to_process_queue"
DELIVERY_MODE_PERSISTENT = 2
NUM_OF_TASK_PER_CONSUMER = 1

app = Flask(__name__, static_url_path = "")

@app.route("/")
def index():
    return "Hello, World!", 200

@app.route("/upload-pdf", methods = [POST])
def add_pdf_on_queue():
    init()
    try:
        f = request.files["pdf_file"]
    except:
        return "Brak pliku", 400

    # logging.warning("NAZWA PLIKU")
    # logging.warning(f.filename)

    if ".pdf" not in f.filename:
        return "Błędny typ pliku", 400

    if(len(f.filename) > 0):

        save_file(f)
        add_to_queue(f.filename)
        
#        channel = open_channel()
#        channel.basic_publish(
#                exchange = "",
#                routing_key = QUEUE_NAME,
#                body = f.filename,
#                properties = pika.BasicProperties(delivery_mode = DELIVERY_MODE_PERSISTENT))

        app.logger.debug(f.filename)
        return "Uploaded file", 201
    else:
        return "Empty uploaded file", 400

def save_file(file_to_save):
    path_to_file = PDFS_DIR_IN_QUEUE + file_to_save.filename
    file_to_save.save(path_to_file)

# def callback_mq(channel, method, properties, body):
#     app.logger.debug("Filename from queue %s" % body)
#     pdf_title = extract_data_from_pdf(body)
#     app.logger.debug(pdf_title)
#     move_file_to_processed_dir(body)
#     app.logger.info("[DONE] File in queue has been processed (%s)" % body)
#     channel.basic_ack(delivery_tag = method.delivery_tag)

# def extract_data_from_pdf(pdf_filename):
#     pdf_title = ""
#     try:
#         src = PDFS_DIR_IN_QUEUE + str(pdf_filename.decode(CODING))
#         pdf = PdfFileReader(open(src, 'rb'))
#         pdf_info = pdf.getDocumentInfo()
#         app.logger.debug(pdf_info)
#         pdf_title = str(pdf_info.title)
#     except:
#         app.logger.error("[ERROR] During extracting data from pdf (%s)" % pdf_filename)

#     return pdf_title

# def move_file_to_processed_dir(filename_in_queue):
#     src = PDFS_DIR_IN_QUEUE + str(filename_in_queue.decode(CODING))
#     dest = PDFS_DIR_PROCESSED + str(filename_in_queue.decode(CODING))
#     try:
#         os.rename(src, dest)
#         app.logger.debug("Moved file %s" % filename_in_queue)
#     except FileNotFoundError as e:
#         app.logger.error(e)

# def consume_messages():
#     channel = open_channel()
#     channel.basic_qos(prefetch_count = NUM_OF_TASK_PER_CONSUMER)
#     channel.basic_consume(queue = QUEUE_NAME, on_message_callback = callback_mq)
#     channel.start_consuming()

# consumer__queue_thread = Thread(target = consume_messages)
# consumer__queue_thread.start()
