document.addEventListener('DOMContentLoaded', function (event) {

    let addingForm = document.getElementById("adding-form");

    addingForm.addEventListener("submit", function (event) {
        event.preventDefault();

        var data = new FormData();
        // data.append("filename", document.getElementById("filename").value);
        data.append("article", document.getElementById("file").files[0]);
        
        var xhttp = new XMLHttpRequest();
        xhttp.addEventListener("load", function () {
            if (xhttp.status == "200") {
                location.reload()
            } 
            console.log(xhttp.status);
        });

        xhttp.open("POST", "http://0.0.0.0:80/article-manager");
        // xhttp.setRequestHeader("Accept", "*/*");
        // xhttp.setRequestHeader("Access-Control-Allow-Origin", "*");
        xhttp.send(data)
    });

});