from flask import Flask, request, render_template, redirect, url_for, send_file
import redis
import sys
# import atexit

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host='redis', port=6379, decode_responses=True)
# atexit.register(exit_handler)

DIR_PATH = "files/"
FILE_COUNTER = "file_counter"
ORG_FILENAME = "org_filename"
NEW_FILENAME = "new_filename"
PATH_TO_FILE = "path_to_file"
FILENAMES = "filenames"

@app.route('/')
def show_articles():
    files_names = db.hvals(FILENAMES)
    hashes = db.hkeys(FILENAMES)
    files = [files_names, hashes]
    return render_template("index.html", my_files = files)

@app.route("/article-manager", methods=["POST"])
def upload_article():
    f = request.files["article"]
    save_file(f)
    return redirect(url_for("show_articles"))

@app.route("/article-manager/<string:article_hash>", methods=["GET"])
def download_article(article_hash):
    full_name = db.hget(article_hash, PATH_TO_FILE)
    org_filename = db.hget(article_hash, ORG_FILENAME)

    if(full_name != None):
        try:
            return send_file(full_name, attachment_filename = org_filename)
        except Exception as e:
            print(e, file = sys.stderr)

    #return org_filename, 200
    return "Nie znaleziono pliku o nazwie\"{}\".".format(article_hash), 404

def save_file(file_to_save):
    if(len(file_to_save.filename) > 0):
        filename_prefix = str(db.incr(FILE_COUNTER))
        new_filename = filename_prefix + file_to_save.filename

        path_to_file = DIR_PATH + new_filename
        file_to_save.save(path_to_file)

        db.hset(new_filename, ORG_FILENAME, file_to_save.filename)
        db.hset(new_filename, PATH_TO_FILE, path_to_file)
        db.hset(FILENAMES, new_filename, file_to_save.filename)
    else:
        print("\n\t\t[WARN] Empty content of file\n", file = sys.stderr)


# def exit_handler():
#     exec("cd /files touch plik.txt")
