from flask import Flask, request
from flask_restplus import Api, Resource, fields
from book import Book

app = Flask(__name__)
api_app = Api(app = app, version = "0.1", title = "Sixth app API", description = "REST-full API for library")

hello_namespace = api_app.namespace("hello", description = "Info API")
book_namespace = api_app.namespace("book", description = "Book API")

authors = []
books = []

@hello_namespace.route("/")
class Hello(Resource):

    @api_app.doc(responses = {200: "OK, Hello World"})
    def get(self):
        return {
                "message": "Hello, World!"
        }

@book_namespace.route("/<int:id>")
class Book(Resource):

    @api_app.doc(responses = {200: "OK", 400: "Invalid argument"},
            params = {"id": "Specify book Id"})
    def get(self, id):
        try:

            return {
                    "message": "Found book by id: {0}".format(id)
            }

        except Exception as e:
            book_namespace.abort(400, e.__doc__, status = "Could not find book by id", statusCode = "400")

    @api_app.doc(responses = {200: "OK", 400: "Invalid argument"},
            params = {"id": "Specify book Id to remove"})
    def delete(self, id):
        try:

            return {
                    "message": "Removed book by id: {0}".format(id)
            }

        except Exception as e:
            book_namespace.abort(400, e.__doc__, status = "Could not remove book by id", statusCode = "400")

@book_namespace.route("/list")
class BookList(Resource):

    new_author_model = api_app.model("Author model",
            {
                "name": fields.String(required = True, description = "Author name", help = "Name cannot be blank"),
                "surname": fields.String(required = True, description = "Author surname", help = "Surname cannot be null") 
            })

    new_book_model = api_app.model("Book model",
            {
                "title": fields.String(required = True, description = "Book title", help = "Title cannot be null", example = "Bieguni"),
                "year": fields.Integer(required = True, description = "Year of publication", help = "Year cannot be null", example = "2007"),
                "author_id": fields.Integer(required = True, description = "Author's Id ", help = "Author's Id cannot be null")
            })

    @api_app.doc(responses = {200: "OK"})
    def get(self):

        return {
                "message": "get all books"
        }

    @api_app.expect(new_book_model)
    def put(self):
        try:
            title = request.json["title"]
            year = request.json["year"]
            author_id = request.json["author_id"]
            result = {
                    "message": "Added new book",
                    "title": title,
                    "year": year,
                    "author_id": author_id
            }

            return result

        except KeyError as e:
            book_namespace.abort(400, e.__doc__, status = "Could not save new book", statusCode = "400")

def get_authors_by_id(author_id):
    for author in authors:
        if(author.id == author_id):
            return author
    return None

def get_book_by_title(title):
    for book in books:
        if(book.title == title):
            return book
    return None