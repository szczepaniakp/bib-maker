from flask import (
    Flask,
    request,
    render_template,
    redirect,
    url_for,
    make_response,
)
import redis
from flask_jwt_extended import (
    JWTManager,
    create_access_token,
    jwt_required,
    get_jwt_identity,
    jwt_refresh_token_required,
    unset_jwt_cookies,
    create_refresh_token,
)
import os
import logging
import crypt
import hashlib
import json

GET = "GET"
POST = "POST"
SESSION_ID = "session-id"
USERNAMES = "usernames"
SESSION_IDS = "sessionids"
SECRET_KEY = "P3_SECRET"
PASSWORD_SALT = "BIB_MAKER_DB_SALT"
TOKEN_EXPIRES_IN_SECONDS = 300
REDIS_SALT = os.environ.get(PASSWORD_SALT)

app = Flask(__name__, static_url_path="")
db = redis.Redis(host="redis", port=6379, decode_responses=True)

app.config["JWT_TOKEN_LOCATION"] = ["cookies"]
app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS

app.config["WTF_CSRF_CHECK_DEFAULT"] = False
app.config["JWT_CSRF_IN_COOKIES"] = False
app.config["JWT_COOKIE_CSRF_PROTECT"] = False

jwt = JWTManager(app)


@app.route("/")
def index():
    response = make_response(render_template("index.html"))

    return response


@app.route("/register", methods=[POST, GET])
def register():

    if request.method == GET:
        return render_template("registration.html")

    elif request.method == POST:
        if str(request.form["password"]) != str(request.form["repeat-password"]):
            return render_template("registration.html", info="Brak zgodności haseł.")

        login = request.form["login"]

        if db.hexists(USERNAMES, login):
            return render_template("registration.html", info="Podany login jest juz zajęty.")

        hashed_password = hashlib.sha512(
            request.form["password"].encode("utf-8")).hexdigest()

        userdata = {
            "id": int(db.hlen(USERNAMES)),
            "password": hashed_password,
            "firstname": request.form["firstname"],
            "surname": request.form["surname"],
            "birthdate": request.form["birthdate"],
            "sex": request.form["sex"],
            "filenames": {},
            "bibs": {}
        }

        json_data = json.dumps(userdata)
        db.hset(USERNAMES, login, json_data)

        return render_template("login.html")


@app.route("/login", methods=[GET, POST])
def login():
    if request.method == POST:
        login = request.form["login"]

        if not db.hexists(USERNAMES, login):
            return render_template("login.html", info="Brak takiego uzytkownika.")

        else:
            userdata = json.loads(db.hget(USERNAMES, login))
            given_password = hashlib.sha512(
                request.form["password"].encode("utf-8")).hexdigest()

            if userdata["password"] != given_password:
                return render_template("login.html", info="Błędne dane logowania. Proszę spróbować ponownie.")

        response = make_response(render_template("index.html"))
        name_hash = hashlib.sha512(login.encode("utf-8")).hexdigest()

        response.set_cookie(
            "access_token_cookie", create_access_token(identity=login), secure=True, httponly=True
        )
        response.set_cookie(
            "refresh_token_cookie", create_refresh_token(identity=login), secure=True, httponly=True
        )
        response.set_cookie(
            SESSION_ID, name_hash, max_age=TOKEN_EXPIRES_IN_SECONDS, secure=True, httponly=True
        )

        return refresh_session_id(response, name_hash)

    elif request.method == GET:
        return render_template("login.html")


@app.route("/logout", methods=[GET])
@jwt_required
def logout():
    session_id = request.cookies.get('session-id')
    response = make_response(redirect(url_for("index")))

    response.set_cookie(
        SESSION_ID, session_id, max_age=0, secure=True, httponly=True
    )
    unset_jwt_cookies(response)
    if db.sismember(SESSION_IDS, str(session_id)):
        db.srem(SESSION_IDS, session_id)
    return response


@app.route('/refresh', methods=[POST])
@jwt_refresh_token_required
@jwt_required
def refresh(response, session_id):
    if session_id is None:
        return page_unauthorized("Sesja wygasła.")
    user = get_jwt_identity()
    response.set_cookie(
        "access_token_cookie", create_access_token(identity=user), secure=True, httponly=True
    )
    return refresh_session_id(response, session_id)


def refresh_session_id(response, current_session_id):
    response.set_cookie(
        SESSION_ID, current_session_id, max_age=TOKEN_EXPIRES_IN_SECONDS, secure=True, httponly=True
    )
    db.set(current_session_id, "will expire in 5 minuts",
           ex=TOKEN_EXPIRES_IN_SECONDS)
    db.sadd(SESSION_IDS, current_session_id)
    return response


@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error=error)


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error=error)


@app.errorhandler(400)
def page_bad_request(error):
    return render_template("errors/400.html", error=error)


@jwt.unauthorized_loader
@app.errorhandler(401)
def page_unauthorized(error):
    return render_template("errors/401.html", error=error)
