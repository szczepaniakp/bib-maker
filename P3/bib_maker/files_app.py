from flask import Flask, request, render_template, make_response, send_file, redirect, url_for
import redis
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity

import os
import sys
import logging
import json
import datetime
from jwt_app import refresh

GET = "GET"
POST = "POST"
SECRET_KEY = "P3_SECRET"
TOKEN_EXPIRES_IN_SECONDS = 300

app = Flask(__name__, static_url_path="")
db = redis.Redis(host="redis", port=6379, decode_responses=True)

USERNAMES = "usernames"
DIR_PATH = "files/"
FILE_COUNTER = "file_counter"
BIB_COUNTER = "bib_counter"
ORG_FILENAME = "org_filename"
NEW_FILENAME = "new_filename"
PATH_TO_FILE = "path_to_file"
FILENAMES = "filenames"
PUBLIC_FILENAMES = "public_filenames"
UPLOAD_DATE = "upload_date"
ORG_BIB_TITLE = "org_bib_title"
BIB_DESC = "bib_description"
AUTHOR = "author"
YEAR = "year"
BIBS = "bibs"

app.config["JWT_TOKEN_LOCATION"] = ["cookies"]
app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS

app.config["WTF_CSRF_CHECK_DEFAULT"] = False
app.config["JWT_CSRF_IN_COOKIES"] = False
app.config["JWT_COOKIE_CSRF_PROTECT"] = False

jwt = JWTManager(app)


@app.route("/files", methods=[GET])
@jwt_required
def files():
    response = make_response(render_template(
        "secure/index-files.html", user_files=dict(), public_files=dict()))

    try:
        public_files = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))
        userdata = json.loads(db.hget(USERNAMES, get_jwt_identity()))
        response = make_response(render_template(
            "secure/index-files.html", user_files=userdata["filenames"], public_files=public_files))
    except:
        logging.warning("Brak plików.")

    return refresh(response, request.cookies.get('session-id'))


@app.route("/bibs", methods=[GET])
@jwt_required
def bibs(files_to_pin={}):
    userdata = json.loads(db.hget(USERNAMES, get_jwt_identity()))

    try:
        files_to_pin = json.loads(request.args['files_to_pin'])
    except:
        logging.warning("Renderowanie bez plików do podpięcia.")

    response = make_response(render_template(
        "secure/index-bibs.html", bibs=userdata[BIBS], files_to_pin=files_to_pin))

    return refresh(response, request.cookies.get('session-id'))


@app.route("/add-bib", methods=[GET, POST])
@jwt_required
def add_bib():
    if request.method == GET:
        response = make_response(render_template("secure/add-bib.html"))
        return refresh(response, request.cookies.get('session-id'))

    elif request.method == POST:
        title = request.form["title"]
        author = request.form["author"]
        year = request.form["year"]
        desc = ""

        try:
            desc = request.form["description"]
        except Exception as e:
            logging.info("Brak opisu pozycji.")

        new_bib_name = str(db.incr(BIB_COUNTER)) + "-" + title
        data = {
            ORG_BIB_TITLE: title,
            AUTHOR: author,
            YEAR: year,
            BIB_DESC: desc,
            UPLOAD_DATE: datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S"),
            FILENAMES: {}
        }

        login = get_jwt_identity()
        userdata = json.loads(db.hget(USERNAMES, login))
        userdata[BIBS][new_bib_name] = data
        db.hset(USERNAMES, login, json.dumps(userdata))

        return redirect(url_for('bibs'))

    else:
        page_bad_request("Niepoprawna metoda.")


@app.route("/add-file", methods=[GET, POST])
@jwt_required
def add_file():
    if request.method == GET:
        response = make_response(render_template("secure/add-file.html"))
        return refresh(response, request.cookies.get('session-id'))

    elif request.method == POST:
        is_public = False
        try:
            is_public = True if str(
                request.form["publicSwitch"]) == "on" else False
        except Exception as e:
            print(str(e))

        save_file(request.files["file"], get_jwt_identity(), is_public)
        return redirect(url_for('files'))
    else:
        page_bad_request("Niepoprawna metoda.")


@app.route("/pin-file/<string:bib_hash>", methods=[POST])
@jwt_required
def pin_file(bib_hash):
    filenames = request.form.getlist('file-selector')
    files_to_pin = {}
    login = get_jwt_identity()
    userdata = json.loads(db.hget(USERNAMES, login))
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    for filename in filenames:
        if filename in userdata[FILENAMES]:
            file = userdata[FILENAMES][filename]
        elif filename in public_data:
            file = public_data[filename]

        files_to_pin[filename] = file

    userdata[BIBS][bib_hash][FILENAMES].update(files_to_pin)
    db.hset(USERNAMES, login, json.dumps(userdata))

    return redirect(url_for('bibs'))


@app.route("/show-files-to-pin/<string:bib_hash>/", methods=[GET])
@jwt_required
def show_files_to_pin(bib_hash):
    login = get_jwt_identity()
    userdata = json.loads(db.hget(USERNAMES, login))
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    data_to_send = {}
    data_to_send.update(userdata[FILENAMES])
    data_to_send.update(public_data)

    already_pinned = json.loads(db.hget(USERNAMES, login))[
        BIBS][bib_hash][FILENAMES]

    for key in already_pinned.keys():
        data_to_send.pop(key, None)

    # bibs(files_to_pin={bib_hash:data_to_send})
    return redirect(url_for('bibs', files_to_pin=json.dumps({bib_hash: data_to_send})))


@app.route("/download-file/<string:file_hash>", methods=[GET])
@jwt_required
def return_files(file_hash):
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    if file_hash in public_data:
        try:
            full_name = public_data[file_hash][PATH_TO_FILE]
            return send_file(full_name)

        except Exception as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie"{}".'.format(file_hash))
    else:
        try:
            userdata = json.loads(db.hget(USERNAMES, get_jwt_identity()))
            full_name = userdata["filenames"][file_hash][PATH_TO_FILE]
            return send_file(full_name)

        except Exception as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie"{}".'.format(file_hash))


@app.route("/delete-file/<string:file_hash>", methods=[GET])
@jwt_required
def delete_file(file_hash):
    public_data = json.loads(db.hget(FILENAMES, PUBLIC_FILENAMES))

    if file_hash in public_data:
        try:
            del public_data[file_hash]
            db.hset(FILENAMES, PUBLIC_FILENAMES, json.dumps(public_data))
            os.system("rm \"" + DIR_PATH + file_hash + "\"")

        except Exception as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie "{}".'.format(file_hash))

    else:
        login = get_jwt_identity()
        userdata = json.loads(db.hget(USERNAMES, login))
        try:
            del userdata[FILENAMES][file_hash]
            db.hset(USERNAMES, login, json.dumps(userdata))
            os.system("rm \"" + DIR_PATH + file_hash + "\"")

        except KeyError as e:
            print(str(e), file=sys.stderr)
            return page_bad_request('Nie znaleziono pliku o nazwie "{}".'.format(file_hash))

    return redirect(url_for('files'))


@app.route("/delete-bib/<string:bib_hash>", methods=[GET])
@jwt_required
def delete_bib(bib_hash):
    login = get_jwt_identity()
    userdata = json.loads(db.hget(USERNAMES, login))

    try:
        del userdata[BIBS][bib_hash]
        db.hset(USERNAMES, login, json.dumps(userdata))

    except KeyError as e:
        logging.warning(str(e))
        return page_bad_request('Pozycja bibliograficzna o nazwie "{}" nie istnieje.'.format(bib_hash))

    return redirect(url_for('bibs'))


@app.route("/unpin-file/<string:bib_hash>/<string:file_hash>", methods=[GET])
@jwt_required
def unpin_file(bib_hash, file_hash):
    login = get_jwt_identity()
    userdata = json.loads(db.hget(USERNAMES, login))

    try:
        del userdata[BIBS][bib_hash][FILENAMES][file_hash]
        db.hset(USERNAMES, login, json.dumps(userdata))

    except KeyError as e:
        logging.warning(str(e))
        return page_bad_request('Pozycja bibliograficzna o nazwie "{}" nie zawiera pliku "{}".'.format(bib_hash, file_hash))

    return redirect(url_for('bibs'))


def save_file(file_to_save, login, is_public=True):
    if len(file_to_save.filename) > 0:

        new_filename = str(db.incr(FILE_COUNTER)) + "-" + file_to_save.filename
        path_to_file = DIR_PATH + new_filename

        data = {
            ORG_FILENAME: file_to_save.filename,
            PATH_TO_FILE: path_to_file,
            UPLOAD_DATE: datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        }

        if is_public:
            public_filenames = db.hget(FILENAMES, PUBLIC_FILENAMES)
            public_data = json.loads(
                public_filenames) if public_filenames is not None else {}
            public_data[new_filename] = data
            db.hset(FILENAMES, PUBLIC_FILENAMES, json.dumps(public_data))

        else:
            userdata = json.loads(db.hget(USERNAMES, login))
            userdata["filenames"][new_filename] = data
            db.hset(USERNAMES, login, json.dumps(userdata))

        file_to_save.save(path_to_file)

    else:
        print("\n\t\t[WARN] Empty content of file\n", file=sys.stderr)


@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error=error)


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error=error)


@app.errorhandler(400)
def page_bad_request(error):
    return render_template("errors/400.html", error=error)


@jwt.unauthorized_loader
@jwt.expired_token_loader
@jwt.needs_fresh_token_loader
@app.errorhandler(401)
def page_unauthorized(error):
    return render_template("errors/401.html", error=error)
