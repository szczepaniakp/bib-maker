from flask import Flask, request, render_template, redirect, url_for, make_response, abort
import redis
import hashlib

POST = "POST"
GET = "GET"
SESSION_ID = "session-id"

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host = "redis", port = 6379, decode_responses = True)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/login", methods = [GET, POST])
def login():
    if(request.method == POST):
        username = request.form["username"].encode("utf-8")
        name_hash = hashlib.sha512(username).hexdigest()
        db.set(SESSION_ID, name_hash)
        response = make_response(render_template("index.html"))
        response.set_cookie(SESSION_ID, name_hash, max_age = 30, secure = False, httponly = True)
        return response

    return render_template("login.html")

@app.route("/secure", methods = [GET])
def secret_page():
    name_hash = request.cookies.get(SESSION_ID)
    if(name_hash != None):
        return render_template("secure/secure-page.html")
    else:
        abort(403)

@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error = error)

@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error = error)
