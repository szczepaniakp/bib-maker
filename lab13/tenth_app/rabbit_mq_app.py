from flask import Flask, request
import pika
import time

GET = "GET"
POST = "POST"
QUEUE_NAME = "msg_queue"
DELIVERY_MODE_PERSISTENT = 2
DELAY = 10
NUM_OF_TASK_PER_CONSUMER = 1


app = Flask(__name__, static_url_path = "")

def open_channel():
    rabbit_mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
    channel = rabbit_mq_connection.channel()
    channel.queue_declare(queue = QUEUE_NAME, durable = True)
    return channel

@app.route("/")
def index():
    return "Hello, World!", 200

@app.route("/add-message", methods = [POST])
def add_message_on_queue():
    message = request.form.get("msg")

    channel = open_channel()
    channel.basic_publish(
            exchange = "", 
            routing_key = QUEUE_NAME, 
            body = message, 
            properties = pika.BasicProperties(delivery_mode = DELIVERY_MODE_PERSISTENT))

    app.logger.debug(message)
    return "Added message", 201

def callback_mq(channel, method, properties, body):
    app.logger.debug("Inside callback_mq()")
    time.sleep(DELAY)
    app.logger.info("Message from queue %s" % body)
    app.logger.info("DONE")
    channel.basic_ack(delivery_tag = method.delivery_tag)


@app.route("/start-consuming-messages", methods = [GET])
def consume_messages():
    channel = open_channel()
    channel.basic_qos(prefetch_count = NUM_OF_TASK_PER_CONSUMER)
    channel.basic_consume(queue = QUEUE_NAME, on_message_callback = callback_mq)
    channel.start_consuming()