from flask import Flask, request, render_template, send_file, abort
import redis
from flask_jwt_extended import JWTManager, create_access_token, jwt_required
import os, sys

GET = "GET"
POST = "POST"
SECRET_KEY = "LAB_5_SECRET"
SESSION_ID = "session-id"
TOKEN_EXPIRES_IN_SECONDS = 300

app = Flask(__name__, static_url_path="")
db = redis.Redis(host="redis-files", port=6379, decode_responses=True)

USERNAMES = "usernames"
DIR_PATH = "files/"
FILE_COUNTER = "file_counter"
ORG_FILENAME = "org_filename"
NEW_FILENAME = "new_filename"
PATH_TO_FILE = "path_to_file"
FILENAMES = "filenames"
SESSION_IDS = "sessionids"


app.config["JWT_TOKEN_LOCATION"] = ["cookies"]
app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS

app.config["WTF_CSRF_CHECK_DEFAULT"] = False
app.config["JWT_CSRF_IN_COOKIES"] = False
app.config["JWT_COOKIE_CSRF_PROTECT"] = False

jwt = JWTManager(app)

@app.route("/")
def index():
    return files()

@app.route("/files", methods=[GET])
@jwt_required
def files():

    if not has_access(request):
        abort(403)
    else:
        chosen_files_ids = []
        return render_template("secure/index-files.html", files=chosen_files_ids)


@app.route("/addfile", methods=[GET, POST])
@jwt_required
def add_file():
    if not has_access(request):
        abort(403)

    if request.method == GET:
        return render_template("secure/add-file.html")
    elif request.method == POST:
        filename = request.files["file"]
        user_session_id = request.cookies.get(SESSION_ID)
        user = db.hget(SESSION_IDS, user_session_id)
        save_file(filename, user)
        return render_template("secure/index-files.html")
    else:
        abort(404)


def has_access(request):
    name_hash = request.cookies.get(SESSION_ID)
    if name_hash != None:
        return True
    else:
        return False


@app.route("/download-files/", methods=[GET])
@jwt_required
def return_files_tut():
    file_hash = request.url()
    full_name = db.hget(file_hash, PATH_TO_FILE)
    org_filename = db.hget(file_hash, ORG_FILENAME)

    if file_id != None:
        try:
            return send_file("files/cos.png")

        except Exception as e:
            return print(str(e), file=sys.stderr)

    return page_bad_request('Nie znaleziono pliku o nazwie"{}".'.format(file_hash))


def save_file(file_to_save, user):
    if len(file_to_save.filename) > 0:
        #db.get(USERNAMES, login)
        inc = hget(user, "files_number")
        hincrby(user, "files_number")
        filename_prefix = str(user)
        new_filename = filename_prefix + file_to_save.filename

        path_to_file = DIR_PATH + new_filename
        file_to_save.save(path_to_file)

        db.hset(new_filename, ORG_FILENAME, file_to_save.filename)
        db.hset(new_filename, PATH_TO_FILE, path_to_file)
        db.hset(FILENAMES, new_filename, file_to_save.filename)
    else:
        print("\n\t\t[WARN] Empty content of file\n", file=sys.stderr)


@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error=error)


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error=error)


@app.errorhandler(400)
def page_bad_request(error):
    return render_template("errors/400.html", error=error)


@app.errorhandler(401)
def page_unauthorized(error):
    return render_template("errors/401.html", error=error)
