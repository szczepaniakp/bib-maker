document.addEventListener('DOMContentLoaded', function (event) {

    const GET = "GET";
    const POST = "POST";

    let registrationForm = document.getElementById("registration-form");
    let loginInput = document.getElementById("login");

    loginInput.addEventListener("change", checkLoginAvailability);

    registrationForm.addEventListener("submit", function (event) {
        event.preventDefault();

        if (event.srcElement[6].value !== event.srcElement[7].value) {
            console.log("Brak zgodności haseł.")
            alert("Hasła są niezgodne.")
        } else {
            buildRequest(event.srcElement)
        }
    });

    function buildRequest(userData) {
        var data = new FormData();
        data.append("firstname", userData[0].value);
        data.append("lastname", userData[1].value);
        data.append("birthdate", userData[2].value);
        data.append("login", userData[3].value);
        data.append("pesel", userData[4].value);
        data.append("sex", userData[5].value);
        data.append("password", userData[6].value);
        data.append("photo", document.getElementById("photo").files[0]);

        var xhttp = new XMLHttpRequest();

        xhttp.addEventListener("load", function (response) {
            if (xhttp.status == "201") {
                alert("Zarejestrowano!")
                console.log("Zarejestrowano!");
            } else {
                alert("Proszę sprawdzić dane w formularzu.")
                console.log("Błędne dane.");
            }
            console.log(xhttp.status);
        });

        xhttp.open(POST, "https://cors-anywhere.herokuapp.com/https://pi.iem.pw.edu.pl/register");// https://cors-anywhere.herokuapp.com/https://
        //xhttp.setRequestHeader("Accept", "*/*");
        xhttp.setRequestHeader("Access-Control-Allow-Origin", "no-cors");
        //xhttp.setRequestHeader("Content-type", "application/json") //
        //xhttp.setRequestHeader("Origin", "http://localhost:80");

        xhttp.setRequestHeader("Access-Control-Allow-Headers", "x-requested-with");
        xhttp.send(data)
    }

    function checkLoginAvailability() {
        let loginInput = document.getElementById("login");
        let baseUrl = "https://pi.iem.pw.edu.pl/user/";
        let userUrl = baseUrl + loginInput.value;

        fetch(userUrl, { 
            method: GET ,
            mode: 'no-cors'
        }).then(function (response) {
            if (response.ok) {
                console.log("Login zajęty.");
                alert('Login zajęty.');
                loginInput.value = ""
            } else {
                console.log("Login wolny.");
            }
            return response.status;

        }).catch(function (error) {
            console.log("Błąd.");
            return error.status;
        });
    }
});