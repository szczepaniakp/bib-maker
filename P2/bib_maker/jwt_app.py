from flask import (
    Flask,
    request,
    render_template,
    redirect,
    url_for,
    make_response,
    abort,
)
import redis
from flask_jwt_extended import (
    JWTManager,
    create_access_token,
    jwt_required,
    set_access_cookies,
    get_jwt_identity,
)
import os
import logging
import crypt
import hashlib

GET = "GET"
POST = "POST"
SESSION_ID = "session-id"
USERNAMES = "usernames"
SESSION_IDS = "sessionids"
SECRET_KEY = "LAB_5_SECRET"
PASSWORD_SALT = "BIB_MAKER_DB_SALT"
TOKEN_EXPIRES_IN_SECONDS = 300
REDIS_SALT = os.environ.get(PASSWORD_SALT)

app = Flask(__name__, static_url_path="")
db = redis.Redis(host="redis", port=6379, decode_responses=True)

app.config["JWT_TOKEN_LOCATION"] = ["cookies"]
app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS

app.config["WTF_CSRF_CHECK_DEFAULT"] = False
app.config["JWT_CSRF_IN_COOKIES"] = False
app.config["JWT_COOKIE_CSRF_PROTECT"] = False

jwt = JWTManager(app)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/register", methods=[POST, GET])
def register():

    logging.warning("W REGISTER")

    if request.method == GET:
        return render_template("registration.html")
    elif request.method == POST:
        login = request.form["login"]
        hashed_password = hashlib.sha512(login.encode("utf-8")).hexdigest()

        userdata = {
            "password": hashed_password,
            "firstname": request.form["firstname"],
            "surname": request.form["surname"],
            "birthdate": request.form["birthdate"],
            "pesel": request.form["pesel"],
            "sex": request.form["sex"],
            "session_id": "",
            "files_number": 0,
        }

        for d in userdata.keys():
            db.hset(login, d, userdata[d])

        db.sadd(USERNAMES, login)

        return render_template("login.html")


@app.route("/login", methods=[GET, POST])
def login():
    if request.method == POST:
        login = request.form["login"]
        username = login.encode("utf-8")

        logging.warning(db.smembers(USERNAMES))

        if not db.sismember(USERNAMES, login):
            return render_template("login.html", info="Brak takiego uzytkownika.")
  
        else:
            given_password = hashlib.sha512(username).hexdigest()
            if db.hget(login, "password") != given_password:
                return render_template("login.html", info="Błędne dane logowania. Proszę spróbować ponownie.")

        name_hash = hashlib.sha512(username).hexdigest()
        response = make_response(render_template("index.html"))
        token = create_access_token(identity=login)

        response.set_cookie(
            SESSION_ID, name_hash, max_age=300, secure=False, httponly=True
        )
        set_access_cookies(response, token)

        db.set(SESSION_ID, name_hash, ex=300)

        return response

    elif request.method == GET:
        return render_template("login.html")


def refresh_token_and_session_id(current_session_id):
    new_session_id = hashlib.sha512(username).hexdigest()
    token = create_access_token(identity=login)
    
    db.set(new_session_id, current_session_id, ex=300, xx=True)
    db.hset(login, SESSION_ID, new_session_id)
    db.set(SESSION_IDS, login, ex=300, xx=True)
    return token, new_session_id


@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error=error)


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error=error)


@app.errorhandler(400)
def page_bad_request(error):
    return render_template("errors/400.html", error=error)


@app.errorhandler(401)
def page_unauthorized(error):
    return render_template("errors/401.html", error=error)
